# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.5.0] - 2021-12-29

## Added

- API test

## [0.4.0] - 2021-12-22

## Added

- Js calculator

## [0.3.0] - 2021-12-17

### Added 

- Grid Garden exercices
- Flex-box Froggy exercicies

## [0.2.0] - 2021-12-16

### Added 

- Bootstrap introduction.
- Tachyons introduction.

## [0.1.1] - 2021-12-15

### Hotfix

- Date fix.

## [0.1.0] - 2021-12-15

###  Added

- git flow basic commands.
- markdown basic commands.

